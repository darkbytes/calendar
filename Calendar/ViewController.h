//
//  ViewController.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFSCalendarView.h"

@interface ViewController : UIViewController<AFSCalendarViewDataSource, AFSCalendarViewDelegate>
{
	AFSCalendarView *_calendar;
}

@end

