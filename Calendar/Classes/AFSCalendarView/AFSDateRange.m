//
//  AFSDateRange.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "AFSDateRange.h"
#import "NSDateExtender.h"

@implementation AFSDateRange

+(AFSDateRange *)rangeFromDate:(NSDate *)start toDate:(NSDate *)end {
	return [[[AFSDateRange alloc] initWithStartDate:start endDate:end] autorelease];
}

-(id)initWithStartDate:(NSDate *)start endDate:(NSDate *)end {
	if (!(self = [super init])) {
		[self dealloc];
		return nil;
	}
	
	if ([start compare:end] != NSOrderedAscending) {
		@throw [[[NSException alloc] initWithName:@"Invalid AFSDateRange" reason:@"Invalid startdate / enddate for AFSDateRange" userInfo:nil] autorelease];
	}
	
	_startDate = [start retain];
	_endDate = [end retain];
	
	return self;
}

-(BOOL)containsDate:(NSDate *)date {
	return [self.startDate isLessOrEqualThanDate:date] && [self.endDate isGreaterThanDate:date];
}

-(NSString *)description {
	return [NSString stringWithFormat:@"%@ %@ to %@", [super description], [self.startDate stringWithFormat:@"dd/MM/yyyy HH:mm::ss"], [self.endDate stringWithFormat:@"dd/MM/yyyy HH:mm:ss"]];
}

@end
