//
//  USCalendarActivity.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFSCalendarActivityDuration.h"

@interface AFSCalendarActivity : NSObject
@property (nonatomic, readonly) NSDate *startDate;
@property (nonatomic, readonly) AFSCalendarActivityDuration *duration;
@property (nonatomic, readonly) NSDate *endDate;
@property (nonatomic, assign) BOOL isLocked; // Set to true if the activity is locked (i.e.: user interations are disabled)
@property (nonatomic, retain) UIView *view;

+(AFSCalendarActivity *)activityWithDate:(NSDate *)date; // default duration is 60 minutes
+(AFSCalendarActivity *)activityWithDate:(NSDate *)date andDuration:(AFSCalendarActivityDuration *)duration;
-(id)initWithDate:(NSDate *)date duration:(AFSCalendarActivityDuration *)duration;

@end
