//
//  USCalendarBox.m
//  PrenotaUnCampo
//
//  Created by TheDoctor
//  Copyright (c) AppForSport. All rights reserved.
//

#import "AFSCalendarBox.h"

@interface AFSCalendarBox (Private)

-(UIColor *)defaultColor;

-(UIColor *)defaultBorderColor;

@end




@implementation AFSCalendarBox

#pragma mark - Public class methods -

+(id)boxWithFrame:(CGRect)frame {
	return [[[AFSCalendarBox alloc] initWithFrame:frame] autorelease];
}



#pragma mark - Lifecycle -

-(id)initWithFrame:(CGRect)frame {
	if ((self = [super initWithFrame:frame])) {
		self.opaque = NO;
		self.userInteractionEnabled = YES;
		self.exclusiveTouch = NO;
		self.dashed = NO;
		self.isSelectable = YES;
		self.borderWidth = 1;
		self.borderColor = [[UIColor colorWithWhite:0 alpha:0.2] retain];
		
		_date = nil;
		_minAlpha = 0.6f;
		_maxAlpha = 1.0f;
		_animationDuration = 3.16f;

		
		_label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		_label.font = [UIFont systemFontOfSize:14];
		_label.textColor = [UIColor blackColor];
		_label.frame = CGRectMake(10, 2, frame.size.width-20, frame.size.height-4);
		_label.textAlignment = NSTextAlignmentCenter;
		_label.lineBreakMode = NSLineBreakByWordWrapping;
		_label.numberOfLines = 0;
		_label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		[self addSubview:_label];
			
		[self setIsSelected:NO animated:NO];
	}
	
  return self;
}

-(void)dealloc {
  [_date release];
  [_borderColor release];
  
  [super dealloc];
}

-(void)setFrame:(CGRect)frame {
  [super setFrame:frame];
  [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect {
	[super drawRect:rect];

	UIColor *fillColor;
	CGFloat glossyLevel;

	if (self.isSelectable) {
		CGFloat red, green, blue, alpha;

		[self.color getRed:&red green:&green blue:&blue alpha:&alpha];
		alpha = self.isSelected ? _maxAlpha : _minAlpha;
		fillColor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
		glossyLevel = _selected ? 0.4f : 0.0f;
	}
	else {
		glossyLevel = self.glossyLevel;
		fillColor = self.color;
	}

	CGFloat radius = 3.f; // corner radius
	CGFloat offset = 2.f; // offset inside rect

	CGFloat lineWidth = self.borderWidth, halfLineWidth = self.borderWidth*0.5f;

	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, fillColor.CGColor);
	CGContextSetStrokeColorWithColor(context, self.borderColor.CGColor);
	CGContextSetLineWidth(context, lineWidth);
	CGContextSetAllowsAntialiasing(context, YES);
	CGMutablePathRef pathRef = CGPathCreateMutable();

	if (self.dashed) {
		CGFloat dashes[] = {4,2};
		CGContextSetLineDash(context, 0, dashes, 2);
	}

	// Path for filling
	CGPathAddArc(pathRef, nil, offset+radius, offset+radius, radius, M_PI, -M_PI_2, 0);
	CGPathAddLineToPoint(pathRef, nil, rect.size.width - offset - radius, offset);
	CGPathAddArc(pathRef, nil, rect.size.width - offset - radius, offset + radius, radius, -M_PI_2, 0, 0);
	CGPathAddRect(pathRef, nil, CGRectMake(offset, offset+radius, rect.size.width - 2*offset, rect.size.height - 2*offset - 2*radius));
	CGPathAddArc(pathRef, nil, rect.size.width - offset - radius, rect.size.height - offset - radius, radius, 0, M_PI_2, 0);
	CGPathAddLineToPoint(pathRef, nil, offset + radius, rect.size.height - offset);
	CGPathAddArc(pathRef, nil, offset + radius, rect.size.height - offset - radius, radius, M_PI_2, -M_PI_2, 0);

	CGContextAddPath(context, pathRef);
	CGContextFillPath(context);

	// add some 3D effect
	if (glossyLevel > 0.) {
		CGContextSaveGState(context);

		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGFloat vLocations[2];
		CGGradientRef gradient;

		// White to clear
		CGFloat vWhiteToClean[8] = {1.0,1.0,1.0,glossyLevel, 1.0,1.0,1.0,0.0};
		vLocations[0] = 0.0;
		vLocations[1] = 1.0;
		gradient = CGGradientCreateWithColorComponents(colorSpace, vWhiteToClean, vLocations, 2);

		CGContextAddPath(context, pathRef);
		CGContextClip(context);
		CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(0, rect.size.height), 0);
		CGGradientRelease(gradient);

		// Clear to black
		CGFloat vCleanToBlack[8] = {1.0,1.0,1.0,0.0, 0.0,0.0,0.0,glossyLevel};
		vLocations[0] = 0.0;
		vLocations[1] = 1.0;
		gradient = CGGradientCreateWithColorComponents(colorSpace, vCleanToBlack, vLocations, 2);
		CGContextAddPath(context, pathRef);
		CGContextClip(context);
		CGContextDrawLinearGradient(context, gradient, CGPointZero, CGPointMake(0, rect.size.height), 0);
		CGGradientRelease(gradient);

		CGColorSpaceRelease(colorSpace);

		CGContextRestoreGState(context);
	}

	CGPathRelease(pathRef);

	// Border line
	CGContextAddArc(context, offset+radius+halfLineWidth, offset+radius+halfLineWidth, radius, M_PI, -M_PI_2, 0);
	CGContextAddLineToPoint(context, rect.size.width-offset-radius-lineWidth, offset+halfLineWidth);
	CGContextAddArc(context, rect.size.width-offset-radius-halfLineWidth, offset+radius+halfLineWidth, radius, -M_PI_2, 0, 0);
	CGContextAddLineToPoint(context, rect.size.width-offset-halfLineWidth, rect.size.height-offset-radius-lineWidth);
	CGContextAddArc(context, rect.size.width-offset-radius-halfLineWidth, rect.size.height-offset-radius-halfLineWidth, radius, 0, M_PI_2, 0);
	CGContextAddLineToPoint(context, offset+radius+halfLineWidth, rect.size.height-offset-halfLineWidth);
	CGContextAddArc(context, offset+radius+halfLineWidth, rect.size.height-offset-radius-halfLineWidth, radius, M_PI_2, M_PI, 0);
	CGContextAddLineToPoint(context, offset+halfLineWidth, offset+radius+halfLineWidth);

	CGContextStrokePath(context);
}



#pragma mark - Private instance methods -

-(UIColor *)defaultColor {
	return [UIColor redColor];
}

-(UIColor *)defaultBorderColor {
	return [UIColor blackColor];
}



#pragma mark - Public methods -

-(void)setColor:(UIColor *)color {
  color = [(color ? : [self defaultColor]) retain];
  [_color release];
  _color = color;
  [self setNeedsDisplay];
}

-(void)setBorderColor:(UIColor *)borderColor {
	if (!borderColor) {
		borderColor = [self defaultBorderColor];
	}
	
	[borderColor retain];
	[_borderColor release];
	_borderColor = borderColor;
	
	[self setNeedsDisplay];
}

-(void)setDashed:(BOOL)dashed {
	_dashed = dashed;
}

-(void)setBorderWidth:(CGFloat)borderWidth {
	_borderWidth = borderWidth;
	[self setNeedsDisplay];
}

-(void)setGlossyLevel:(CGFloat)level {
	_glossyLevel = level >= 0 && level <= 1 ? level : (level < 0 ? 0 : 1);
	[self setNeedsDisplay];
}

-(void)setIsSelectable:(BOOL)isSelectable {
	if (isSelectable != _isSelectable) {
		_isSelectable = isSelectable;
		[self setNeedsDisplay];
	}
}

-(void)setIsSelected:(BOOL)isSelected {
	[self setIsSelected:isSelected animated:NO];
}

-(void)setIsSelected:(BOOL)selected animated:(BOOL)animated {
	_isSelected = selected;
	
	if (animated) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:_animationDuration];
	}
  
	[self setNeedsDisplay];
  
	if (animated) {
		[UIView commitAnimations];
	}
}

-(void)setAccessoryView:(UIView *)accessoryView {
	if (accessoryView == _accessoryView) {
		return;
	}
	
	if (accessoryView) {
		[_accessoryView removeFromSuperview];
		_accessoryView = accessoryView;
		_accessoryView.center = CGPointMake(accessoryView.frame.size.width*0.5f + 10, self.frame.size.height*0.5f);
		[self addSubview:_accessoryView];
	}
	else {
		[_accessoryView removeFromSuperview];
		_accessoryView = nil;
	}
}

@end
