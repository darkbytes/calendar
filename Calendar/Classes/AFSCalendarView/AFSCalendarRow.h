//
//  USCalendarViewRow.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AFSCalendarRow;

typedef enum {
	AFSCalendarRowSideTop,
	AFSCalendarRowSideBottom
} AFSCalendarRowSide;

typedef enum {
	AFSCalendarRowFillSideNone    = 0,
	AFSCalendarRowFillSideTop     = 1,
	AFSCalendarRowFillSideBottom  = 2,
	AFSCalendarRowFillSideBoth    = 3
} AFSCalendarRowFillSide;

@interface AFSCalendarRow : UITableViewCell
{
	UIView *_leftView;
	UILabel *_timeLabel1, *_timeLabel2;
}
@property (nonatomic, assign) BOOL dashedLine;
@property (nonatomic, assign) AFSCalendarRowFillSide fillSide;
@property (nonatomic, retain) UIColor *fillColor;
@property (nonatomic, retain) NSDate *date;

@end
