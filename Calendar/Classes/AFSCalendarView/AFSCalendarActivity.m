//
//  USCalendarActivity.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "AFSCalendarActivity.h"

@interface AFSCalendarActivity (Private)

-(UIColor *)defaultColor;

@end



@implementation AFSCalendarActivity

#pragma mark - Public class methods -

+(AFSCalendarActivity *)activityWithDate:(NSDate *)date {
	return [self activityWithDate:date andDuration:[AFSCalendarActivityDuration durationWithMinutes:60]];
}

+(AFSCalendarActivity *)activityWithDate:(NSDate *)date andDuration:(AFSCalendarActivityDuration *)duration {
	return [[[AFSCalendarActivity alloc] initWithDate:date duration:duration] autorelease];
}



#pragma mark - Lifecycle -

-(id)initWithDate:(NSDate *)date duration:(AFSCalendarActivityDuration *)duration {
	if (!date) {
		@throw [NSException exceptionWithName:@"Invalid date" reason:@"An activity date cannot be nil" userInfo:nil];
	}
	
	if (!duration) {
		@throw [NSException exceptionWithName:@"Missing duration" reason:@"An activity duration cannot be nil" userInfo:nil];
	}
	
	if (!(self = [super init])) {
		[self dealloc];
		return nil;
	}
	
	_startDate = [date retain];
	_duration = [duration retain];
	_endDate = [[date dateByAddingTimeInterval:_duration.minutes*60] retain];
	
	return self;
}

-(void)dealloc {
	[_startDate release];
	[_endDate release];
	[_duration release];
	
	[super dealloc];
}



#pragma mark - Private instance methods -

-(UIColor *)defaultColor {
	return [UIColor colorWithWhite:0 alpha:0.2];
}



#pragma mark - Public instance methods -

-(NSString *)description {
	return [NSString stringWithFormat:@"Calendar activity with date: %@, duration: %li", [self.startDate description], (long)self.duration.minutes];
}

@end
