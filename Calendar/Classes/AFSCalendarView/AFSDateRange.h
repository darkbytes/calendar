//
//  AFSDateRange.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFSDateRange : NSObject
@property (nonatomic, readonly, nonnull) NSDate *startDate, *endDate;

+(nullable AFSDateRange *)rangeFromDate:(nonnull NSDate *)start toDate:(nonnull NSDate *)end;
-(nullable id)initWithStartDate:(nonnull NSDate *)start endDate:(nonnull NSDate *)end;

-(BOOL)containsDate:(nonnull NSDate *)date;

@end
