//
//  AFSCalendarView.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "AFSCalendarView.h"
#import "NSDateExtender.h"
#import "AFSCalendarRow.h"

@interface AFSCalendarView(Private)

-(nonnull AFSDateRange *)adjustRange:(nonnull AFSDateRange *)range usingDuration:(NSTimeInterval)duration;

-(NSInteger)numberOfIntervalsAtIndex:(NSUInteger)index usingDuration:(NSTimeInterval)duration;

-(nonnull NSDate *)dateAtIndex:(NSInteger)index;

-(nonnull NSDate *)dateAtIndexPath:(NSIndexPath *)indexPath;

-(void)onTableViewCellTap:(UITapGestureRecognizer *)tap;

-(void)onSelectionBoxTap:(UITapGestureRecognizer *)tap;

-(UIView *)createSelectionBoxForView:(UIView *)view atIndexPath:(NSIndexPath *)indexPath duration:(NSTimeInterval)duration;

@end



@implementation AFSCalendarView

-(id)initWithFrame:(CGRect)frame {
	if (!(self = [super initWithFrame:frame])) {
		[self dealloc];
		return nil;
	}
	
	_ranges = [[NSMutableArray alloc] init];
	
	_table = [[[UITableView alloc] initWithFrame:self.bounds] autorelease];
	_table.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	_table.dataSource = self;
	_table.delegate = self;
	_table.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self addSubview:_table];
	
	self.interval = 30*60;
	self.rowHeight = 50;
	self.dateFormat = @"yyyy, MMMM dd";
	self.colorForActiveDates = [UIColor colorWithWhite:0.97 alpha:1.0];
	self.colorForInactiveDates = [UIColor whiteColor];
	
	return self;
}

-(void)dealloc {
	[_ranges release];
	
	[super dealloc];
}

-(void)willMoveToSuperview:(UIView *)newSuperview {
	[super willMoveToSuperview:newSuperview];
	
	if (newSuperview) {
		[self reloadData];
	}
}

-(void)willMoveToWindow:(UIWindow *)newWindow {
	[super willMoveToWindow:newWindow];
	
	if (newWindow) {
		[self reloadData];
	}
}



#pragma mark - Private instance methods -

-(nonnull AFSDateRange *)adjustRange:(AFSDateRange *)range usingDuration:(NSTimeInterval)duration {
	NSInteger minutes = duration/60;
	
	NSDateComponents *theComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:range.startDate];
	NSDate *startDate = [range.startDate dateWithHour:theComponents.hour minute:theComponents.minute-(theComponents.minute%minutes) second:0];
	
	theComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:range.endDate];
	
	if (theComponents.minute % minutes) {
		theComponents.minute += minutes-(theComponents.minute%minutes);
		
		if (theComponents.minute == 0) {
			theComponents.hour++;
		}
	}
	
	NSDate *endDate = [range.endDate dateWithHour:theComponents.hour minute:theComponents.minute second:0];
	
	return [AFSDateRange rangeFromDate:startDate toDate:endDate];
}

-(NSInteger)numberOfIntervalsAtIndex:(NSUInteger)index usingDuration:(NSTimeInterval)duration {
	AFSDateRange *range = _ranges[index];
	return [range.endDate timeIntervalSinceDate:range.startDate] / duration + 1;
}

-(nonnull NSDate *)dateAtIndex:(NSInteger)index {
	return [[_ranges objectAtIndex:index] startDate];
}

-(nonnull NSDate *)dateAtIndexPath:(NSIndexPath *)indexPath {
	return [[self dateAtIndex:indexPath.section] dateByAddingTimeInterval:self.interval*indexPath.row];
}

-(void)onTableViewCellTap:(UITapGestureRecognizer *)tap {
	if (![self.delegate respondsToSelector:@selector(AFSCalendarView:userDidTouchAtDate:)]) {
		return;
	}
	
	UITableViewCell *cell = (UITableViewCell *)tap.view;
	NSIndexPath *indexPath = [_table indexPathForCell:cell];
	AFSDateRange *range = _ranges[indexPath.section];
	NSDate *date = [self dateAtIndexPath:indexPath];
	
	CGPoint touchPoint = [tap locationInView:tap.view];
	BOOL touchBottomSide = touchPoint.y < cell.frame.size.height*0.5f;
	
	if (touchBottomSide) {
		date = [date dateByAddingTimeInterval:-self.interval];
	}
	
	if ([date isLessThanDate:range.startDate] || [date isGreaterThanDate:range.endDate] || (!touchBottomSide && [date isEqualToDate:range.endDate])) {
		return;
	}
	
	[self.delegate AFSCalendarView:self userDidTouchAtDate:date];
}

-(void)onSelectionBoxTap:(UITapGestureRecognizer *)tap {
	[self hideSelectionBox];
}

-(UIView *)createSelectionBoxForView:(UIView *)view atIndexPath:(NSIndexPath *)indexPath duration:(NSTimeInterval)duration {
	CGRect rect = [_table rectForRowAtIndexPath:indexPath];
	rect.origin.y += rect.size.height*0.5f;
	rect.origin.x = 50;
	rect.size.width -= 70;
	rect.size.height = self.rowHeight*(duration/self.interval) - 1;
	
	UIView *box = [[[UIView alloc] initWithFrame:rect] autorelease];
	box.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	box.layer.zPosition = 0;
	
	view.frame = box.bounds;
	view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	[box addSubview:view];
	
	return box;
}



#pragma mark - UITableViewDataSource/Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return _ranges.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self numberOfIntervalsAtIndex:section usingDuration:self.interval];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	AFSCalendarRow *cell = [tableView dequeueReusableCellWithIdentifier:@"row"];
	
	if (!cell) {
		cell = [[[AFSCalendarRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"row"] autorelease];
		cell.fillColor = self.colorForActiveDates;
		
		UITapGestureRecognizer *tap = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTableViewCellTap:)] autorelease];
		[cell addGestureRecognizer:tap];
	}
	
	AFSDateRange *range = _ranges[indexPath.section];
	NSDate *date = [range.startDate dateByAddingTimeInterval:indexPath.row*self.interval];
	cell.date = date;
	cell.fillSide = AFSCalendarRowFillSideNone;
	
	if (self.delegate) {
		if ([self.delegate respondsToSelector:@selector(AFSCalendarView:shouldUseDashedLineForDate:)]) {
			cell.dashedLine = [self.delegate AFSCalendarView:self shouldUseDashedLineForDate:date];
		}
		
		if ([self.delegate respondsToSelector:@selector(AFSCalendarView:isDateActive:)]) {
			NSDate *previousDate = [date dateByAddingTimeInterval:-self.interval];
			
			if ([self.delegate AFSCalendarView:self isDateActive:date]) {
				cell.fillSide = AFSCalendarRowFillSideBottom;
				
				if ([self.delegate AFSCalendarView:self isDateActive:previousDate]) {
					cell.fillSide += AFSCalendarRowFillSideTop;
				}
			}
			else {
				if ([self.delegate AFSCalendarView:self isDateActive:previousDate]) {
					cell.fillSide = AFSCalendarRowFillSideTop;
				}
			}
			
		}
	}
	
	return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	AFSDateRange *range = _ranges[section];
	return [range.startDate stringWithFormat:self.dateFormat];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *view = nil;
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(AFSCalendarView:viewForDayTitleAtIndex:)]) {
		view = [self.delegate AFSCalendarView:self viewForDayTitleAtIndex:section];
		
		if (!view) {
			@throw [[[NSException alloc] initWithName:@"Invalid view" reason:@"AFSCalendarView:viewForDayTitleAtIndex: cannot return a nil view" userInfo:nil] autorelease];
		}
	}
	else {
		view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)] autorelease];
		view.backgroundColor = [UIColor lightGrayColor];
		
		UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(5, 0, view.frame.size.width-5, view.frame.size.height)] autorelease];
		label.textAlignment = NSTextAlignmentCenter;
		label.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
		label.textColor = [UIColor whiteColor];
		label.text = [self tableView:tableView titleForHeaderInSection:section];
		[view addSubview:label];
	}
	
	return view;
}



#pragma mark - Public instance methods -

-(UIScrollView *)scrollView {
	return _table;
}

-(void)setInterval:(NSTimeInterval)interval {
	if ((NSInteger)interval%15) {
		@throw [[[NSException alloc] initWithName:@"Bad calendar interval" reason:@"An interval must be multiple of 5" userInfo:nil] autorelease];
	}
	
	_interval = interval;
}

-(void)setRowHeight:(CGFloat)rowHeight {
	_table.rowHeight = rowHeight;
}

-(CGFloat)rowHeight {
	return _table.rowHeight;
}

-(void)reloadData {
	[_ranges removeAllObjects];
	
	if (self.dataSource) {
		NSUInteger days = [self.dataSource numberOfDaysInAFSCalendarView:self];
		NSTimeInterval intv = self.interval;
		
		for (NSUInteger i=0; i<days; i++) {
			AFSDateRange *range = [self.dataSource AFSCalendarView:self dateLimitsAtIndex:i];
			AFSDateRange *adjustedRange = [self adjustRange:range usingDuration:intv];
			[_ranges addObject:adjustedRange];
		}
	}
	
	[_table reloadData];
}

-(void)hideSelectionBox {
	[self hideSelectionBoxAnimated:YES];
}

-(void)hideSelectionBoxAnimated:(BOOL)animated {
	if (!_selection_box) {
		return;
	}
	
	if (animated) {
		UIView *theBox = _selection_box;
		_selection_box = nil;
		
		[UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
			theBox.transform = CGAffineTransformMakeScale(0.95, 0.95);
			theBox.alpha = 0;
		} completion:^(BOOL finished) {
			if (finished) {
				[theBox removeFromSuperview];
			}
		}];
	}
	else {
		[_selection_box removeFromSuperview];
		_selection_box = nil;
	}
}

-(void)showSelectionBoxAtDate:(NSDate *)date duration:(NSTimeInterval)duration {
	if (!self.delegate) {
		return;
	}
	
	[self hideSelectionBox];
	
	UIView *customBox = [self.delegate AFSCalendarView:self viewForSelectionBoxAtDate:date duration:duration];
	
	for (NSInteger section=0; section < _ranges.count; section++) {
		AFSDateRange *range = _ranges[section];
		
		if ([range containsDate:date]) {
			NSInteger row = [date timeIntervalSinceDate:range.startDate] / self.interval;
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
			
			UIView *box = [self createSelectionBoxForView:customBox atIndexPath:indexPath duration:duration];
			
			UITapGestureRecognizer *selectionBoxTap = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSelectionBoxTap:)] autorelease];
			[box addGestureRecognizer:selectionBoxTap];
			
			[_table addSubview:box];
			_selection_box = box;
		}
	}
}

@end
