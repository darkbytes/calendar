//
//  USCalendarViewRow.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "AFSCalendarRow.h"

@interface AFSCalendarRow (Private)

-(UIColor *)defaultFillColor;

@end



@implementation AFSCalendarRow

#pragma mark - Lifecycle -

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
		self.backgroundColor = [UIColor clearColor];
		self.fillColor = self.defaultFillColor;
		self.selectionStyle = UITableViewCellSelectionStyleNone;
		
		_leftView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, self.frame.size.height)] autorelease];
		_leftView.backgroundColor = [UIColor whiteColor];
		_leftView.autoresizingMask = (UIViewAutoresizingFlexibleHeight);
		[self addSubview:_leftView];
		
		_timeLabel1 = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, _leftView.frame.size.height)] autorelease];
		_timeLabel1.font = [UIFont systemFontOfSize:15];
		_timeLabel1.textColor = [UIColor colorWithWhite:0.7f alpha:1.0f];
		_timeLabel1.textAlignment = NSTextAlignmentRight;
		_timeLabel1.autoresizingMask = UIViewAutoresizingFlexibleHeight;
		_timeLabel1.center = CGPointMake(_leftView.frame.size.width - _timeLabel1.frame.size.width*0.5f - 5, _leftView.frame.size.height*0.5f);
		_timeLabel1.adjustsFontSizeToFitWidth = YES;
		[_leftView addSubview:_timeLabel1];
	}
	
	return self;
}

-(void)dealloc {
	[_fillColor release];
	[super dealloc];
}

-(void)drawRect:(CGRect)rect {
	[super drawRect:rect];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetLineWidth(context, 0.5f);
	CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor);
	
	CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
	CGContextFillRect(context, rect);
	
	if (self.fillSide != AFSCalendarRowFillSideNone) {
		CGRect fillRect = CGRectMake(0, 0, rect.size.width-20, 0);
		
		switch (self.fillSide) {
			case AFSCalendarRowFillSideNone: // just to remove the warning
				break;
				
			case AFSCalendarRowFillSideTop:
				fillRect.size.height = rect.size.height*0.5f;
				break;
				
			case AFSCalendarRowFillSideBoth:
				fillRect.size.height = rect.size.height;
				break;
				
			case AFSCalendarRowFillSideBottom:
				fillRect.origin.y = rect.size.height*0.5f;
				fillRect.size.height = rect.size.height*0.5f;
				break;
		}
		
		CGContextSetFillColorWithColor(context, _fillColor.CGColor);
		CGContextFillRect(context, fillRect);
	}
	
	if (_dashedLine) {
		CGFloat dashes[] = {1, 2};
		CGContextSetLineDash(context, 0, dashes, 2);
	}
	
	// Drawing a line
	CGContextMoveToPoint(context, 0, rect.size.height*0.5f);
	CGContextAddLineToPoint(context, rect.size.width, rect.size.height*0.5f);
	CGContextStrokePath(context);
}



#pragma mark - Private instance methods -

-(UIColor *)defaultFillColor {
	return [UIColor colorWithRed:28./255. green:125./255. blue:181./255. alpha:1];
}



#pragma mark - Public instance methods -

-(void)setDate:(NSDate *)date {
	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	formatter.dateFormat = @"HH:mm";
	_timeLabel1.text = [formatter stringFromDate:date];
}

-(void)setDashedLine:(BOOL)dashedLine {
	if ((_dashedLine = dashedLine)) {
		_timeLabel1.font = [UIFont systemFontOfSize:10];
		_timeLabel1.textColor = [UIColor colorWithWhite:0.7f alpha:1.0f];
	}
	else {
		_timeLabel1.font = [UIFont systemFontOfSize:12];
		_timeLabel1.textColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
	}
	
	[self setNeedsDisplay];
}

-(void)setFillSide:(AFSCalendarRowFillSide)fillSide {
	_fillSide = fillSide;
	[self setNeedsDisplay];
}

-(void)setFillColor:(UIColor *)fillColor {
	if (fillColor == _fillColor) {
		return;
	}
	
	if (!fillColor) {
		fillColor = self.defaultFillColor;
	}
	
	[fillColor retain];
	[_fillColor release];
	_fillColor = fillColor;
}

@end
