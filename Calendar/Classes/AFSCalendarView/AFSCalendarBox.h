//
//  USCalendarBox.h
//  PrenotaUnCampo
//
//  Created by TheDoctor
//  Copyright (c) AppForSport. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class USCalendarBox;

@interface AFSCalendarBox : UIView
{
  UILabel *_label;
  CGFloat _lineWidth;
  NSDate *_date;
  BOOL _dashed, _selected;
  CGFloat _minAlpha, _maxAlpha; // Da usare quando l'oggetto è deselezionato/selezionato
  CGFloat _animationDuration;
}

@property (nonatomic, retain) UIColor *color, *borderColor;
@property (nonatomic, readonly) UILabel *textLabel;
@property (nonatomic, assign) CGFloat borderWidth;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, assign) CGFloat glossyLevel;
@property (nonatomic, assign) BOOL dashed;
@property (nonatomic, assign) BOOL isSelectable;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) UIView *accessoryView;

+(id)boxWithFrame:(CGRect)frame;

-(void)setIsSelected:(BOOL)selected animated:(BOOL)animated;

@end
