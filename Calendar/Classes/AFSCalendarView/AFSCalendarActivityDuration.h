//
//  AFSCalendarActivityDuration.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFSCalendarActivityDuration : NSObject
@property (nonatomic, readonly) NSUInteger minutes;

+(AFSCalendarActivityDuration *)durationWithMinutes:(NSUInteger)minutes;
+(AFSCalendarActivityDuration *)durationWithHours:(NSUInteger)hours;

-(id)initWithMinutes:(NSUInteger)minutes;

@end
