//
//  AFSCalendarView.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFSCalendarActivity.h"
#import "AFSDateRange.h"

@class AFSCalendarView; // forward declare



@protocol AFSCalendarViewDataSource <NSObject>

@required

-(NSUInteger)numberOfDaysInAFSCalendarView:(nonnull AFSCalendarView *)calendarView;
-(nonnull AFSDateRange *)AFSCalendarView:(nonnull AFSCalendarView *)calendarView dateLimitsAtIndex:(NSInteger)index;

@end



@protocol AFSCalendarViewDelegate <NSObject>

@required

-(nonnull UIView *)AFSCalendarView:(nonnull AFSCalendarView *)calendarView viewForSelectionBoxAtDate:(nonnull NSDate *)date duration:(NSTimeInterval)duration;


@optional

-(nonnull UIView*)AFSCalendarView:(nonnull AFSCalendarView *)calendarView viewForDayTitleAtIndex:(NSInteger)index;

-(BOOL)AFSCalendarView:(nonnull AFSCalendarView *)calendarView shouldUseDashedLineForDate:(nonnull NSDate *)date;
-(BOOL)AFSCalendarView:(nonnull AFSCalendarView *)calendarView isDateActive:(nonnull NSDate *)date;
-(void)AFSCalendarView:(nonnull AFSCalendarView *)calendarView userDidTouchAtDate:(nonnull NSDate *)date;

@end



@interface AFSCalendarView : UIView<UITableViewDataSource, UITableViewDelegate>
{
	UITableView *_table;
	NSMutableArray *_ranges;
	UIView *_selection_box;
}
@property (nonatomic, assign, nullable) id<AFSCalendarViewDataSource> dataSource;
@property (nonatomic, assign, nullable) id<AFSCalendarViewDelegate> delegate;
@property (nonatomic, assign) NSTimeInterval interval;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, retain, nonnull) NSString *dateFormat;
@property (nonatomic, retain, nonnull) UIColor *colorForActiveDates, *colorForInactiveDates;
@property (nonatomic, readonly, nonnull) UIScrollView *scrollView;

-(void)reloadData;

-(void)hideSelectionBox;
-(void)hideSelectionBoxAnimated:(BOOL)animated;
-(void)showSelectionBoxAtDate:(nonnull NSDate *)date duration:(NSTimeInterval)duration;

@end
