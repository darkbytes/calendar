//
//  AFSCalendarActivityDuration.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "AFSCalendarActivityDuration.h"

@implementation AFSCalendarActivityDuration

+(AFSCalendarActivityDuration *)durationWithHours:(NSUInteger)hours {
	return [self durationWithMinutes:60*hours];
}

+(AFSCalendarActivityDuration *)durationWithMinutes:(NSUInteger)minutes {
	return [[[AFSCalendarActivityDuration alloc] initWithMinutes:minutes] autorelease];
}

-(id)initWithMinutes:(NSUInteger)minutes {
	if (!(self = [super init])) {
		[self dealloc];
		return nil;
	}
	
	if (!minutes || (minutes%30 != 0)) {
		@throw [[[NSException alloc] initWithName:@"Bad calendar activity duration" reason:@"A calendar activity duration must be a multiple of 30 minutes" userInfo:nil] autorelease];
	}
	
	_minutes = minutes;
	
	return self;
}

@end
