//
//  NSDateExtender.m
//  iBurraco v3
//
//  Created by TheDoctor on 07/11/12.
//  Copyright (c) 2012 Antonio Seprano. All rights reserved.
//

#import "NSDateExtender.h"

@implementation NSDate (Extender)
@dynamic expired;

+(NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format {
	NSDate *result = nil;
  
	if (string && (id)string != [NSNull null]) {
		NSDateFormatter *theFormatter = [[[NSDateFormatter alloc] init] autorelease];
		[theFormatter setDateFormat:format];
		result = [theFormatter dateFromString:string];
	}
	
	return result;
}

-(NSString *)stringWithFormat:(NSString *)format {
	NSDateFormatter *theFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[theFormatter setDateFormat:format];
  
	return [theFormatter stringFromDate:self];
}

-(NSDate *)dateByAddingDayInterval:(NSInteger)day {
	NSDateComponents *theComponents = [[[NSDateComponents alloc] init] autorelease];
	theComponents.day = day;
	return [[NSCalendar currentCalendar] dateByAddingComponents:theComponents toDate:self options:0];
}

-(NSDate *)dateByAddingMonthInterval:(NSUInteger)month {
	NSDateComponents *theComponents = [[[NSDateComponents alloc] init] autorelease];
	theComponents.month = month;
	return [[NSCalendar currentCalendar] dateByAddingComponents:theComponents toDate:self options:0];
}

-(NSDate *)dateWithHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second {
	NSDateComponents *theComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
	theComponents.hour = hour;
	theComponents.minute = minute;
	theComponents.second = second;
	
	return [[NSCalendar currentCalendar] dateFromComponents:theComponents];
}

-(NSDate *)midnight {
	return [self dateWithHour:0 minute:0 second:0];
}

-(NSDate *)nextDay {
	return [self dateByAddingDayInterval:1];
}

-(BOOL)isLessThanDate:(NSDate *)date {
	return [self compare:date] == NSOrderedAscending;
}

-(BOOL)isLessOrEqualThanDate:(NSDate *)date {
	return [self compare:date] != NSOrderedDescending;
}

-(BOOL)isGreaterThanDate:(NSDate *)date {
	return [self compare:date] == NSOrderedDescending;
}

-(BOOL)isGreaterOrEqualThanDate:(NSDate *)date {
	return [self compare:date] != NSOrderedAscending;
}

-(BOOL)isBeetweenDate:(NSDate *)minDate andDate:(NSDate *)maxDate {
	return [self isBeetweenDate:minDate andDate:maxDate strictCheck:YES];
}

-(BOOL)isBeetweenDate:(NSDate *)minDate andDate:(NSDate *)maxDate strictCheck:(BOOL)strict {
	if (strict) {
		return [self isGreaterOrEqualThanDate:minDate] && [self isLessOrEqualThanDate:maxDate];
	}
	
	return [self isGreaterThanDate:minDate] && [self isLessThanDate:maxDate];
}

-(BOOL)expired {
	return [self isLessOrEqualThanDate:[NSDate date]];
}

@end
