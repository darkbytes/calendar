//
//  NSDateExtender.h
//  iBurraco v3
//
//  Created by TheDoctor on 07/11/12.
//  Copyright (c) 2012 Antonio Seprano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extender)
@property(nonatomic, readonly) BOOL expired;

+(NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)format;

-(NSString *)stringWithFormat:(NSString *)format;

-(NSDate *)dateByAddingDayInterval:(NSInteger)day;

-(NSDate *)dateWithHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;

-(NSDate *)midnight;

-(NSDate *)nextDay;

-(BOOL)isLessThanDate:(NSDate *)date;

-(BOOL)isLessOrEqualThanDate:(NSDate *)date;

-(BOOL)isGreaterThanDate:(NSDate *)date;

-(BOOL)isGreaterOrEqualThanDate:(NSDate *)date;

-(BOOL)isBeetweenDate:(NSDate *)minDate andDate:(NSDate *)maxDate;

-(BOOL)isBeetweenDate:(NSDate *)minDate andDate:(NSDate *)maxDate strictCheck:(BOOL)strict;

@end
