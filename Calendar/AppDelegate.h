//
//  AppDelegate.h
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

