//
//  ViewController.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "ViewController.h"
#import "NSDateExtender.h"

@implementation ViewController

-(id)init {
	if (!(self = [super init])) {
		[self dealloc];
		return nil;
	}
	
	self.title = @"Calendar test";
	
	return self;
}

-(void)loadView {
	_calendar = [[[AFSCalendarView alloc] initWithFrame:[UIScreen mainScreen].bounds] autorelease];
	_calendar.dataSource = self;
	_calendar.delegate = self;
	_calendar.colorForActiveDates = [UIColor colorWithRed:199./255. green:231./255. blue:242./255. alpha:0.6];
	_calendar.dateFormat = @"dd/MM/yyyy";
	
	self.view = _calendar;
}



#pragma mark - AFSCalendarViewDataSource -

-(NSUInteger)numberOfDaysInAFSCalendarView:(AFSCalendarView *)calendarView {
	return 5;
}

-(AFSDateRange *)AFSCalendarView:(AFSCalendarView *)calendarView dateLimitsAtIndex:(NSInteger)index {
	NSDate *day = [[NSDate date] dateByAddingDayInterval:index];
	NSDate *startDate = [day dateWithHour:8 minute:0 second:0];
	NSDate *endDate = [day dateWithHour:23 minute:30 second:0];
	
	return [AFSDateRange rangeFromDate:startDate toDate:endDate];
}



#pragma mark - AFSCalendarViewDelegate -

-(BOOL)AFSCalendarView:(AFSCalendarView *)calendarView shouldUseDashedLineForDate:(NSDate *)date {
	NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitMinute fromDate:date];
	return components.minute == 30;
}

-(BOOL)AFSCalendarView:(AFSCalendarView *)calendarView isDateActive:(NSDate *)date {
	return [date isGreaterOrEqualThanDate:[date dateWithHour:9 minute:30 second:0]] && [date isLessThanDate:[date dateWithHour:22 minute:0 second:0]];
}

-(nonnull UIView *)AFSCalendarView:(AFSCalendarView *)calendarView viewForSelectionBoxAtDate:(NSDate *)date duration:(NSTimeInterval)duration {
	UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
	
	UIVisualEffectView *blurView = [[[UIVisualEffectView alloc] initWithEffect:effect] autorelease];
	blurView.frame = CGRectMake(0, 0, 200, 200);
	blurView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
	blurView.layer.cornerRadius = 10;
	blurView.layer.borderWidth = 1;
	blurView.layer.borderColor = [UIColor colorWithWhite:0.3 alpha:0.4].CGColor;
	blurView.layer.masksToBounds = YES;
	blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	
	UIView *view = [[[UIView alloc] initWithFrame:blurView.bounds] autorelease];
	view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	[view addSubview:blurView];
	
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectInset(view.bounds, 10, 10)] autorelease];
	label.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	label.textAlignment = NSTextAlignmentCenter;
	label.textColor = [UIColor colorWithWhite:0.2 alpha:1];
	label.font = [UIFont fontWithName:@"Helvetica-Neue" size:22];
	label.text = [date stringWithFormat:@"HH:mm"];
	label.layer.shadowColor = [UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
	label.layer.shadowOffset = CGSizeMake(0, 1.5);
	label.layer.shadowRadius = 1.;
	label.layer.shadowOpacity = 1.;
	[view addSubview:label];
	
	return view;
}

-(void)AFSCalendarView:(AFSCalendarView *)calendarView userDidTouchAtDate:(NSDate *)date {
	[calendarView showSelectionBoxAtDate:date duration:60*60];
}

@end
