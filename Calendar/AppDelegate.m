//
//  AppDelegate.m
//  Calendar
//
//  Created by TheDoctor
//  Copyright © Antonio Seprano. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "AFSDateRange.h"


@implementation AppDelegate

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	self.window = [[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds] autorelease];
	
	UIViewController *controller = [[[ViewController alloc] init] autorelease];
	self.window.rootViewController = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
	
	[self.window makeKeyAndVisible];
	
	return YES;
}

@end
